"""
初始化项目
    1、实例化app
    2、实例化数据库
    3、数据库绑定app
"""
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# path =  os.path.join(
#         os.path.dirname(
#             os.path.abspath(__file__)
#         ),"otm.sqlite"
#     )

# path =  os.path.join(
#         os.path.dirname(
#             os.path.abspath(__file__)
#         ),"tto.sqlite"
#     )

# path =  os.path.join(
#         os.path.dirname(
#             os.path.abspath(__file__)
#         ),"mtm.sqlite"
#     )
path =  os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
        ),"oto.sqlite"
    )
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///"+path
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

db = SQLAlchemy(app)
