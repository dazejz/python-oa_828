from app import db

class BaseModel(db.Model):
    __abstract__ = True #当前类只是被继承，不会直接执行
    id = db.Column(db.Integer,primary_key = True,autoincrement = True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        db.session.commit()

class PersonInfo(BaseModel):
    __tablename__ = "personinfo"
    p_age = db.Column(db.Integer)
    info_person = db.relationship(
        "Person",
        backref = "person_info",
        uselist = False
    )

class Person(BaseModel):
    __tablename__ = "person"
    p_name = db.Column(db.String(32))
    p_info = db.Column(db.Integer,db.ForeignKey("personinfo.id"))

db.create_all()

#插入人
# p = Person()
# p.p_name = "张三"
# p.save()

#插入详情
# i = PersonInfo()
# i.p_age = 18
# i.save()

#关联关系
# p = Person.query.get(1)
# i = PersonInfo.query.get(1)
# i.info_person = p
# i.save()

#查询个人详情
p = Person.query.get(1)
print(p.person_info.p_age)
#查询详情的主人
i = PersonInfo.query.get(1)
print(i.info_person.p_name)
