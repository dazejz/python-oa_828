from flask import render_template

from app import app
from models import db
from models import Person



@app.route("/")
def home():
    return render_template("index.html")


@app.route("/profile.html")
def profile():
    return render_template("profile.html")


@app.route("/login.html")
def login():
    return render_template("login.html")


@app.route("/department.html")
def department():
    return render_template("department.html", **locals())


@app.route("/detail_news.html")
def detail_news():
    no_img_talk_shit = 'static\img\shangshi.jpg'
    return render_template("detail_news.html", **locals())


@app.route("/news.html")
def news():
    return render_template("news.html", **locals())


@app.route("/detail.html")
def detail():
    return render_template("detail.html")


@app.route("/edit_base.html")
def edit_base():
    return render_template("edit_base.html")


@app.route("/position.html")
def position():
    return render_template("position.html")


@app.route("/position_permission.html")
def position_permission():
    return render_template("position_permission.html")


@app.route("/attendance_me.html")
def attendance_me():
    return render_template("attendance_me.html")


@app.route("/attendance_subordinate.html")
def attendance_subordinate():
    return render_template("attendance_subordinate.html")


@app.route("/edit_person.html")
def edit_person():
    return render_template("edit_person.html")


@app.route("/add_permission.html")
def add_permission():
    return render_template("add_permission.html")


@app.route("/add_person.html")
def add_person():
    return render_template("add_person.html")


@app.route("/edit_permission.html")
def edit_permission():
    return render_template("edit_permission.html")


@app.route("/index.html")
def index():
    return render_template("index.html")


@app.route("/person.html")
def person():
    return render_template("person.html")


@app.route("/permission.html")
def permission():
    return render_template("permission.html")


@app.route("/add_news.html")
def add_news():
    return render_template("add_news.html")


@app.route("/edit_news.html")
def edit_news():
    return render_template("edit_news.html")


@app.route("/add_department.html")
def add_department():
    return render_template("add_department.html")


@app.route("/edit_department.html")
def edit_department():
    return render_template("edit_department.html")


@app.route("/detail_person.html")
def detail_person():
    return render_template("detail_person.html")


@app.route("/p_list/<string:dep>/<int:page>/")
def p_list(dep,page):
    """
    每页10条
    :param dep:
    :param page:
    :return:
    """
    department_list = db.session.query(Person.department).group_by(Person.department).all()
    department_list = [i[0] for i in department_list]
    page_size = 10
    offset = (page-1)*page_size
    person_list = Person.query.filter(Person.department == dep).limit(10).offset(offset)
    return render_template("p_list.html",**locals())
