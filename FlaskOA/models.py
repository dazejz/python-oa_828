"""
定义数据库模型
"""
from app import db

class BaseModel(db.Model):
    __abstract__ = True #当前类只是被继承，不会直接执行
    id = db.Column(db.Integer,primary_key = True,autoincrement = True)

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        db.session.commit()

class Person(BaseModel):
    name = db.Column(db.String(32))
    age = db.Column(db.Integer)
    gender = db.Column(db.String(8))
    department = db.Column(db.String(32))
    position = db.Column(db.String(32))
    money = db.Column(db.Float)


