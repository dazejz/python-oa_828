from flask import Flask
from flask import render_template
import pymysql

app = Flask(__name__)

@app.route("/library/<string:book_type>/")
def index(book_type):
    #连接数据库
    connect = pymysql.connect(
        host="localhost",
        user="root",
        password="123456",
        database="test")
    #执行操作
    cursor = connect.cursor()
    sql = "select * from library where book_type='{}'".format(book_type)
    exec = cursor.execute(sql) #执行操作，返回受影响数据条数
    result = cursor.fetchall() #查询的结果  fetchall fetchone fetchmany
    #提交操作
    cursor.close()
    connect.commit() #connect.roll_back
    connect.close()
    return render_template("index.html",result = result)

if __name__ == '__main__':
    app.run(port = 80,debug = True)
