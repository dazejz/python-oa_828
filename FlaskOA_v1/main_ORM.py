import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

#创建应用
app = Flask(__name__)

#应用进行配置，数据库配置
# app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:@127.0.0.1/oatest"
path =  os.path.join(
        os.path.dirname(
            os.path.abspath(__file__)
        ),"db.sqlite"
    )
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///"+path

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
#绑定数据库操作实例到app
db = SQLAlchemy(app)

#
class Person(db.Model):
    id = db.Column(db.Integer,primary_key = True,autoincrement = True)
    name = db.Column(db.String(32))

db.create_all()






